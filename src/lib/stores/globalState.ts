import { writable } from 'svelte/store';
import type { MemberState, State } from '$lib/util/types';

const INITIAL_STATE: State = {
	members: new Map<string, MemberState>()
};

export const dropboxReady = writable(false);
export const state = writable(INITIAL_STATE);

export function load(loadedState: State) {
	state.set(loadedState);
}

export function addMember(newMember: string) {
	state.update((curr) => {
		curr.members.set(newMember, { currentValue: 0, invested: 0 });
		return {
			members: curr.members
		};
	});
}

export function removeMember(member: string) {
	state.update((curr) => {
		curr.members.delete(member);
		return {
			members: curr.members
		};
	});
}

export function updateInvestment(member: string, value: number) {
	state.update((curr) => {
		curr.members.set(member, {
			currentValue: (curr.members.get(member)?.currentValue || 0) + +value,
			invested: (curr.members.get(member)?.invested || 0) + +value
		});
		return {
			members: curr.members
		};
	});
}

export function updatePercentage(value: number) {
	state.update((curr) => {
		curr.members.forEach((currVal, member) => {
			curr.members.set(member, {
				currentValue: currVal.currentValue + currVal.currentValue * (+value / 100.0),
				invested: currVal.invested
			});
		});
		return {
			members: curr.members
		};
	});
}
