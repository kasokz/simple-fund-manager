export const CODE_VERIFIER_COOKIE = 'code_verifier';
export const REFRESH_TOKEN_COOKIE = 'refresh_token';

export function getDropboxAuthURLFromURL(url: URL) {
	return `${url.protocol}//${url.host}/dropbox-auth`;
}

export function getRedirectUriFromURL(url: URL, shouldOpenDialogAfterRedirect: boolean) {
	return `${url.protocol}//${url.host}/dropbox-auth/redirect${
		shouldOpenDialogAfterRedirect ? '?open-dialog=true' : ''
	}`;
}
