export interface State {
	members: Map<string, MemberState>;
}

export interface MemberState {
	invested: number;
	currentValue: number;
}
