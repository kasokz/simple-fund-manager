import { AUTH_CLIENT_ID } from '$env/static/private';
import {
	CODE_VERIFIER_COOKIE,
	REFRESH_TOKEN_COOKIE,
	getRedirectUriFromURL
} from '$lib/util/dropbox.js';
import { redirect } from '@sveltejs/kit';
import { DropboxAuth } from 'dropbox';

export async function GET({ fetch, url, cookies }) {
	const dropboxAuth = new DropboxAuth({
		fetch,
		clientId: AUTH_CLIENT_ID
	});

	const shouldOpenDialogAfterRedirect = url.searchParams.get('open-dialog') === 'true';

	if (cookies.get(REFRESH_TOKEN_COOKIE)) {
		return new Response(null, {
			status: 200
		});
	}
	const code = url.searchParams.get('code');
	if (code && cookies.get(CODE_VERIFIER_COOKIE)) {
		try {
			dropboxAuth.setCodeVerifier(cookies.get(CODE_VERIFIER_COOKIE) ?? '');
			const token = await dropboxAuth.getAccessTokenFromCode(
				getRedirectUriFromURL(url, shouldOpenDialogAfterRedirect),
				code
			);
			const result = token.result as Record<string, string>;
			cookies.delete(CODE_VERIFIER_COOKIE, { path: '/' });
			cookies.set(REFRESH_TOKEN_COOKIE, result.refresh_token, { path: '/' });
		} catch (error) {
			console.error(error);
			throw error;
		}
		throw redirect(302, `/${shouldOpenDialogAfterRedirect ? '?open-dialog=true' : ''}`);
	} else {
		return new Response(null, {
			status: 401
		});
	}
}
