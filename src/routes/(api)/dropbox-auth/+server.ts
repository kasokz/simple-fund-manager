import { DropboxAuth } from 'dropbox';
import { AUTH_CLIENT_ID } from '$env/static/private';
import { redirect } from '@sveltejs/kit';
import { CODE_VERIFIER_COOKIE, getRedirectUriFromURL } from '$lib/util/dropbox.js';

export async function GET({ fetch, url, cookies }) {
	const dropboxAuth = new DropboxAuth({
		fetch,
		clientId: AUTH_CLIENT_ID
	});

	const shouldOpenDialogAfterRedirect = url.searchParams.get('open-dialog') === 'true';

	const authUrl = await dropboxAuth.getAuthenticationUrl(
		getRedirectUriFromURL(url, shouldOpenDialogAfterRedirect),
		undefined,
		'code',
		'offline',
		['files.metadata.read', 'files.metadata.write', 'files.content.write', 'files.content.read'],
		'none',
		true
	);
	cookies.set(CODE_VERIFIER_COOKIE, dropboxAuth.getCodeVerifier(), {
		path: '/'
	});

	throw redirect(302, authUrl.toString());
}
