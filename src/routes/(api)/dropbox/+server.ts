import { AUTH_CLIENT_ID } from '$env/static/private';
import { REFRESH_TOKEN_COOKIE, getDropboxAuthURLFromURL } from '$lib/util/dropbox.js';
import { json, redirect } from '@sveltejs/kit';
import { Dropbox, DropboxAuth, type files } from 'dropbox';

export interface DropboxFileEntry {
	fileName: string;
	lastModified: string;
	revision: string;
}

export interface DropboxFileUpload {
	fileContent: string;
	fileName: string;
}

export async function GET({ cookies, url, fetch }) {
	const dropboxAuth = new DropboxAuth({
		fetch,
		clientId: AUTH_CLIENT_ID
	});
	if (!cookies.get(REFRESH_TOKEN_COOKIE)) {
		throw redirect(303, `${getDropboxAuthURLFromURL(url)}?open-dialog=true`);
	}
	dropboxAuth.setRefreshToken(cookies.get(REFRESH_TOKEN_COOKIE) || '');
	const dropbox = new Dropbox({
		auth: dropboxAuth,
		fetch,
		clientId: AUTH_CLIENT_ID
	});
	const response = await dropbox.filesListFolder({ path: '', recursive: true });
	return json(
		response.result?.entries
			.map((entry) => ({
				fileName: entry.name,
				lastModified: (entry as files.FileMetadataReference).server_modified,
				revision: (entry as files.FileMetadataReference).rev
			}))
			.sort((a, b) => +new Date(b.lastModified) - +new Date(a.lastModified))
	);
}

export async function POST({ cookies, url, fetch, request }) {
	const dropboxAuth = new DropboxAuth({
		fetch,
		clientId: AUTH_CLIENT_ID
	});
	if (!cookies.get(REFRESH_TOKEN_COOKIE)) {
		throw redirect(303, getDropboxAuthURLFromURL(url));
	}
	dropboxAuth.setRefreshToken(cookies.get(REFRESH_TOKEN_COOKIE) || '');
	const dropbox = new Dropbox({
		auth: dropboxAuth,
		fetch,
		clientId: AUTH_CLIENT_ID
	});
	const { fileName, fileContent }: DropboxFileUpload = await request.json();
	try {
		await dropbox.filesUpload({
			path: `/${fileName}`,
			contents: fileContent,
			mode: { '.tag': 'add' },
			autorename: true
		});
		return new Response(null, {
			status: 200
		});
	} catch (err) {
		return new Response(null, {
			status: 400
		});
	}
}
