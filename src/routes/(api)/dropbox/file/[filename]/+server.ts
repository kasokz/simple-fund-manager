import { AUTH_CLIENT_ID } from '$env/static/private';
import { REFRESH_TOKEN_COOKIE, getDropboxAuthURLFromURL } from '$lib/util/dropbox.js';
import { json, redirect } from '@sveltejs/kit';
import { Dropbox, DropboxAuth } from 'dropbox';
import nodeFetch from 'node-fetch';

export interface DropboxDownloadResult {
	fileBinary: Buffer;
}

export async function GET({ cookies, url, params }) {
	const dropboxAuth = new DropboxAuth({
		fetch: nodeFetch,
		clientId: AUTH_CLIENT_ID
	});
	if (!cookies.get(REFRESH_TOKEN_COOKIE)) {
		throw redirect(303, getDropboxAuthURLFromURL(url));
	}
	dropboxAuth.setRefreshToken(cookies.get(REFRESH_TOKEN_COOKIE) || '');
	const dropbox = new Dropbox({
		auth: dropboxAuth,
		fetch: nodeFetch,
		clientId: AUTH_CLIENT_ID
	});
	const response = await dropbox.filesDownload({ path: `/${params.filename}` });
	return json((response.result as unknown as DropboxDownloadResult).fileBinary.toString());
}
