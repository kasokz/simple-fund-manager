import { REFRESH_TOKEN_COOKIE } from '$lib/util/dropbox.js';

export function load({ cookies, url }) {
	return {
		dropboxAuthenticated: cookies.get(REFRESH_TOKEN_COOKIE) !== undefined,
		openDropboxChooserDialogOnStart: url.searchParams.get('open-dialog') === 'true'
	};
}
