/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'text': '#020209',
        'background': '#fbfbfe',
        'primary-button': '#992d1e',
        'secondary-button': '#ffffff',
        'accent': '#1e992d',
      },
    },
  },
  plugins: [],
}

